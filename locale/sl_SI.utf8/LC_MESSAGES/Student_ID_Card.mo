��          �      �       H     I  "   Z      }  &   �     �     �     �     �     �     �       n        �  �  �     �  %   �  /   �  5   �     4  !   9     [     g     p     v  *   �  y   �  $   -                                             
   	                 Background Image Convert Student ID Cards to Images Download Student ID Cards as zip Generate ID Card for Selected Students Left Left and right (pixels) Photo Position Right Student ID Card Student ID Card Text To print in high quality (144dpi) and at true size, save the cards as an image, and print to 50&percnt; scale. Top and bottom (pixels) Project-Id-Version: Student ID Card module
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:42+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Slika Ozadja Pretvorite dijaške izkaznice v slike Prenesite študentske dijaške kot zip datoteke Générer les cartes pour les élèves sélectionnés Levo Levo in desno (v slikovnih pikah) Fotografija Pozicija Desno ID kartica dijaka Ustvari osebno izkaznico za izbrane dijake Če želite tiskati v visoki kakovosti (144 dpi) in v pravi velikosti, shranite kartice kot sliko in natisnite na 50 %. . Zgoraj in spodaj (v slikovnih pikah) 