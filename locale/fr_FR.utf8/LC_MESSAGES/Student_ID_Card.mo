��          �      <      �     �     �  "   �      �  &        :     ?     W     k     q          �     �     �     �  n   �     /  �  G     ?  '   M  *   u  $   �  5   �     �               2  '   8     `     i     p     �  $   �  �   �     T                         	                                                 
                           Background Image Card Padding Convert Student ID Cards to Images Download Student ID Cards as zip Generate ID Card for Selected Students Left Left and right (pixels) Max. width (pixels) Photo Photo Padding Position Right Student ID Card Student ID Card Text Text Padding To print in high quality (144dpi) and at true size, save the cards as an image, and print to 50&percnt; scale. Top and bottom (pixels) Project-Id-Version: Student ID Card module
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:41+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Image de fond Marge intérieure (padding) de la carte Convertir les cartes d'étudiant en images Télécharger les cartes d'étudiant Générer les cartes pour les élèves sélectionnés Gauche Gauche et droite (pixels) Largeur max. (pixels) Photo Marge intérieure (padding) de la photo Position Droite Carte d'étudiant Texte de la carte d'étudiant Marge intérieure (padding) du texte Pour imprimer en haute qualité (144dpi), et à taille réelle, sauvegardez les cartes comme image, et imprimez à une échelle de 50&percnt;. Haut et bas (pixels) 